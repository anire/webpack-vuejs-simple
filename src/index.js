import Vue from 'vue';
import App from './App.vue';

let vm = new Vue({
    el: "#vuejs",
    render: h => h(App)
})
