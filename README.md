# Simple Vue.js and Webpack example [![build status](https://gitlab.com/anire/webpack-vuejs-simple/badges/master/build.svg)](https://gitlab.com/anire/webpack-vuejs-simple/commits/master)
If you're looking for the bundles, go to the main page, click Downloads, and download 'build' for production or 'dev-build' for development.
To execute a development server, run `npm start`.

**Before you tinker with this project, `npm install`.**

[Live version (production mode, not debuggable)](https://anire.gitlab.io/webpack-vuejs-simple)